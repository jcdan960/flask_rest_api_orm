from routes.home import app
from routes.items import delete_item, get_item, get_items, add_item, update_item

if __name__ == '__main__':
    app.run()
