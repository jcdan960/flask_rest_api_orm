from .base_object import Base, DBHandler, user_name, password
from sqlalchemy import Column, Integer,  Numeric, TEXT
import uuid
from sqlalchemy.dialects.postgresql import UUID

table_name = 'item'


class Item(Base):
    __tablename__ = table_name
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column('name', TEXT)
    category = Column('category', TEXT)
    price = Column('price', Numeric(19, 2))
    quantity = Column('quantity', Integer)

    def __init__(self, id, name, category, price, quantity):
        self.id = id
        self.name = name
        self.category = category
        self.price = price
        self.quantity = quantity

    def to_dict(self):
        return {'id': str(self.id),
                'name': self.name,
                'category': self.category,
                'price': float(self.price),
                'quantity': int(self.quantity)}


handler = DBHandler(table_name, user_name, password)