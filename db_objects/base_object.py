from sqlalchemy.ext.declarative import declarative_base

DB_NAME = 'flask'
user_name = 'flask_user'
password = 'patate123'

Base = declarative_base()


# https://auth0.com/blog/sqlalchemy-orm-tutorial-for-python-developers/


class DBHandler:
    def __init__(self, table_name, user_name, password):
        self.table_name = table_name
        self.user_name = user_name
        self.password = password

    def get_engine_str(self, DB_name):
        connection_str = f'postgresql://{self.user_name}:{self.password}@localhost:5432/{DB_name}'
        print(connection_str)
        return connection_str

