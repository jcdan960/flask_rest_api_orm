from .base_object import Base
from sqlalchemy import Column, TEXT, ForeignKey, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
#https://flask-oauthlib.readthedocs.io/en/latest/oauth2.html

# registered user on your site
class User(Base):
    __tablename__ = 'user'
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column('name', TEXT)


#the app which wants to use the resource of a user
class Client(Base):
    __tablename__ = 'client'
    client_id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column('name', TEXT)
    client_secret = Column('secret', TEXT)
    user_id = Column(UUID, ForeignKey('user.id'), nullable=False)


#A grant token is created in the authorization flow and will be destroyed when the authorization is finished
class GrantToken(Base):
    __tablename__ = 'grant_token'
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    client_id = Column(UUID, ForeignKey('client.id'), nullable=False)
    code = Column(TEXT, nullable=False)
    expiration = Column(DateTime, nullable=False)
    redirect_uri = Column(TEXT, nullable=False)


#A bearer token is the final token that could be used by the client.
class BearerToken(Base):
    __tablename__ = 'bearer_token'
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    client_id = Column(UUID, ForeignKey('client.id'), nullable=False)

    token_type = Column(TEXT) #Bearer only supported
    access_token = Column(TEXT(255), unique=True, nullable=False)
    refresh_token = Column(TEXT(255), unique=True, nullable=False)
    expiration = Column(DateTime, nullable=False)
    _scopes = Column(TEXT, nullable=False)
