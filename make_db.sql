DROP DATABASE flask;
CREATE DATABASE flask;
\c flask;

CREATE TABLE item(
    id UUID NOT NULL PRIMARY KEY UNIQUE,
    name TEXT NOT NULL UNIQUE,
    category TEXT NOT NULL,
    price NUMERIC(19,2) NOT NULL,
    quantity INTEGER NOT NULL
);

CREATE TABLE users(
    id UUID NOT NULL PRIMARY KEY UNIQUE,
    name TEXT NOT NULL UNIQUE
);

CREATE TABLE client(
    id UUID NOT NULL PRIMARY KEY UNIQUE,
    name TEXT NOT NULL,
    client_secret TEXT NOT NULL,
    user_id UUID REFERENCES users(id)
);

CREATE TABLE grant_token(
    grant_id UUID NOT NULL PRIMARY KEY UNIQUE,
    client_id UUID NOT NULL REFERENCES client(id),
    code TEXT NOT NULL,
    expiration TIMESTAMP NOT NULL,
    redirect_uri TEXT NOT NULL
);

CREATE TABLE bearer_token(
    bearer_id UUID NOT NULL PRIMARY KEY UNIQUE,
    client_id UUID NOT NULL REFERENCES client(id),
    token_type TEXT NOT NULL,
    access_token TEXT NOT NULL,
    refresh_token TEXT NOT NULL,
    expiration TIMESTAMP NOT NULL,
    scope TEXT NOT NULL
);

CREATE USER flask_user WITH PASSWORD 'patate123';
GRANT ALL PRIVILEGES ON DATABASE flask TO flask_user;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to flask_user;
