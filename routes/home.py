from flask import jsonify
from flask_uuid import FlaskUUID

from flask import Flask
from sqlalchemy.orm import sessionmaker

from sqlalchemy import create_engine
from db_objects.items_object import handler
from db_objects.base_object import DB_NAME, Base

flask_uuid = FlaskUUID()
app = Flask(__name__)
flask_uuid.init_app(app)

engine = create_engine(handler.get_engine_str(DB_NAME))
engine.connect()
print("connected to DB")

Session = sessionmaker(bind=engine)
session = Session()
Base.metadata.create_all(engine)

@app.route('/')
def home():
    return jsonify('Welcome to my Flask REST API!')

