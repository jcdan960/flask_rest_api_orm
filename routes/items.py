import flask

from .home import app, session
from flask import Response, request
import json
from db_objects.items_object import Item
import uuid
import math
from sqlalchemy import func
MAX_ITEM_PER_REQUEST = 50

@app.route('/api/v1/item/<uuid:id>', methods=['DELETE'])
def delete_item(id):
    print(f'looking for id {id}')
    out = session.query(Item).filter_by(id=id)
    if not out:
        return Response(f"Could not find an item with the id {id}", status=404, mimetype='application/json')
    else:
        out.delete()
        session.commit()
        return Response("Resource deleted", status=204, mimetype='application/json')



@app.route('/api/v1/item/<uuid:id>', methods=['GET'])
def get_item(id):
    print(f'looking for id {id}')
    out = session.query(Item).filter_by(id=id).first()
    if not out:
        return Response(f"Could not find an item with the id {id}", status=404, mimetype='application/json')
    return json.dumps(out.to_dict())


@app.route('/api/v1/items', methods=['GET'])
def get_items():
    print(f'Looking for all items')
    page = request.args.get('page')

    if not page:
        return Response('Page parameter is required', status=400)

    total_nb_items = session.query(func.count(Item.id)).scalar()
    max_page = math.ceil(total_nb_items/ MAX_ITEM_PER_REQUEST)
    if int(page) > max_page:
        return Response('Page parameter is too high', status=400)

    items = session.query(Item).limit(MAX_ITEM_PER_REQUEST).offset(MAX_ITEM_PER_REQUEST*(int(page)-1)).all()

    pages_dict = {'total_pages': max_page, 'current_page': page}

    items_list = [i.to_dict() for i in items]

    return flask.jsonify(pages_dict, items_list)


@app.route('/api/v1/item', methods=['POST'])
def add_item():
    payload = request.get_json()
    print("Processing post add_item")
    new_item = make_item_from_request(payload)
    session.add(new_item)
    try:
        error = session.commit()
    except Exception as e:
        return Response(f"Conflict occurred when saving data: {e}", status=409, mimetype='application/json')

    return Response("Success", status=201, mimetype='application/json')



@app.route('/api/v1/item/<uuid:id>', methods=['PUT', 'PATCH'])
def update_item(id):
    print(f'looking for id {id}')
    out = session.query(Item).filter_by(id=id)
    if not out.first():
        return Response(f"Could not find an item with the id {id}", status=404, mimetype='application/json')

    payload = request.get_json()
    if not payload:
        return Response(f"Cannot update item with this payload", status=400, mimetype='application/json')
    else:
        print("Processing PUT add_item")
        new_item = make_item_from_request(payload)
        out.delete()
        session.add(new_item)
        session.commit()
        return Response("Updated", status=200, mimetype='application/json')


def make_item_from_request(json_data):
    id = uuid.uuid4()
    item = Item(id, json_data["name"], json_data["category"], json_data["price"], json_data["quantity"])
    return item