#https://flask-oauthlib.readthedocs.io/en/latest/oauth2.html

from .home import app

from flask_oauthlib.provider import OAuth2Provider
oauth = OAuth2Provider(app)
